package com.learn.springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.web.access.intercept.AuthorizationFilter;

@SpringBootApplication
@ComponentScan("com.learn.springsecurity.controller")
public class Springsecurity02Application {



	public static void main(String[] args) {
		SpringApplication.run(Springsecurity02Application.class, args);
	}

}
