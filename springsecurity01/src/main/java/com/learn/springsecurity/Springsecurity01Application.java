package com.learn.springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.learn.springsecurity.controller")
public class Springsecurity01Application {

	public static void main(String[] args) {
		SpringApplication.run(Springsecurity01Application.class, args);
	}

}
