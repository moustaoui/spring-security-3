package com.learn.springsercurity.controller;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author smoustaoui
 */
@RestController
public class AccountController {

    @GetMapping("/myAccount")
    public String getAccountDetails() {

        return "Here are the account details from DB";
    }

}
