package com.learn.springsercurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author smoustaoui
 */
@RestController
public class LoansController {

    @GetMapping("/myLoans")
    public String getLoansDetails() {
        return "Here are the loan details from DB";
    }
}
