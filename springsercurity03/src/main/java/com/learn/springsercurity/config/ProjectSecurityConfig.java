package com.learn.springsercurity.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author smoustaoui
 */
@Configuration
public class ProjectSecurityConfig {

    /**
     * Default security configurations
     * @param http
     * @return
     * @throws Exception
     */
    //@Bean
    SecurityFilterChain defaultSecurityFilterChain01(HttpSecurity http) throws Exception {
        ((AuthorizeHttpRequestsConfigurer.AuthorizedUrl)http.authorizeHttpRequests().anyRequest()).authenticated();
        http.formLogin();
        http.httpBasic();
        return (SecurityFilterChain)http.build();
    }

    /**
     * Custom seucirty configurations
     * @param http
     * @return
     * @throws Exception
     */
    //@Bean
    SecurityFilterChain defaultSecurityFilterChain02(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers("/myAccount", "/myBalance", "/myLoans", "myCards").authenticated()
                .requestMatchers("/notices", "/contact").permitAll()
                .and().formLogin()
                .and().httpBasic();
        return http.build();
    }

    /**
     * Configuration to deny all requests
     * @param http
     * @return
     * @throws Exception
     */
    //@Bean
    SecurityFilterChain defaultSecurityFilterChain03(HttpSecurity http) throws Exception {

        http.authorizeHttpRequests()
              .anyRequest().denyAll()
        .and().formLogin()
        .and().httpBasic();
        return http.build();
    }

    /**
     * Configurations to permit all requests
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {

        http.authorizeHttpRequests()
                .anyRequest().permitAll()
                .and().formLogin()
                .and().httpBasic();
        return http.build();
    }

}
