package com.learn.springsercurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan("com.learn.springsecurity.controller")
public class Springsercurity03Application {

	public static void main(String[] args) {
		SpringApplication.run(Springsercurity03Application.class, args);
	}

}
